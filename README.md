#xubuntu-minimal-amd64

Скрипт настраивает свежеустановленный дистрибутив 'xubuntu-minimal-amd64'

Скачать значек Xubuntu_amd64_Codeberg.desktop кликнув по нему скачаеться пакет настройки системы.

wget https://codeberg.org/Chebur/Xubuntu_amd64/raw/branch/main/Xubuntu-minimal-amd64_Codeberg.desktop

chmod +x Xubuntu-minimal-amd64_Codeberg.desktop

Подключиться к интернету по кабелю. Обращаю внимние, при работе скрипта будет скачено и установлено более 300 пакетов. 

Разархивировать каталог 'Xubuntu_amd64_Codeberg.tar.gz' поместить скрипты в домашний каталог (/home/Ваше имя/) из другого места скрипты не сработают.

Процесс настройки длительный, поэтому ,отключить в настройках Менеджера питания автоматическое выключение и автоматический вход в ждущий режим.

По выбору можно установить 'Themes Grub'  'Plymout themes Percentage' и т.д.

1:Запустить скрипт: от root " sudo sh applications.sh "  

2:В меню найдёте значок 'USudoroot' с помощью его запустите скрипт и обновите пароль 'root'

3:Далее Запустить скрипт: ' sudo sh temp.sh   '

4:Далее в меню найдёте значок 'Softinstall' с помощью его запустите скрипт-программу установки 

Появиться окно для выбора программ выбирайте что Вам нужно.

Все программы перечислены в фале ' README.sh '

Длительность работы скрипта может быть продолжительной, 

многое зависит от скорости интернета,выбора программ и быстродействия компьютера. 

В процессе работы скрипта будут выводитья подсказки.

Если 'Внешний вид' не установиться,запустить скрипт  " sh appearance.sh "

Перезагрузите компьютер.

После перезагрузки обновить кнопкой 'UpgradePrelink'

Удачи!

Способ удаления программ установленые скриптом находяться в папке 'Uninstall'	

#https://laban.lysator.liu.se/ubuntu-dvd/xubuntu/daily-minimal/current/
#oracular-minimal-2024-May-2amd64.iso
22badb8f976a7b6b736dda002a193962de26d0ab17207b89afe47e0b4b2dab32 *oracular-minimal-2024-May-29-amd64.iso
#https://laban.lysator.liu.se/ubuntu-dvd/xubuntu/noble/daily-minimal/current/
#noble-minimal-2024-May-28-amd64.iso
67eabe11553bee5bcfe1a3e78823edaa7efc0a4fea4bc41015a2729cca6de400 *noble-minimal-2024-May-28-amd64.iso
#https://cdimage.ubuntu.com/xubuntu/releases/noble/release/
#xubuntu-24.04-minimal-2024-04-24-amd64.iso
09a80f22051ca998954a8df5a43c21897d0c2a3dccb0955e8bc2423725c98612 *xubuntu-24.04-minimal-	2024-04-24-amd64.iso

############################################### 
 



